# Build and Run Application

- To run the application you need to do the following:
    - Install
        - docker.
        - docker docker compose.
    - After installation, use the command line to go to the project root folder.
    - Run the following command "docker-compose up".
    - Wait until application be ready.
    - Use the url "http://localhost:8080/swagger-ui/" to access the swagger and test the API.