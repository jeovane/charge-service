FROM maven:latest AS build
COPY src /usr/app/src
COPY pom.xml /usr/app
WORKDIR /usr/app/
RUN mvn clean package -DskipTests

FROM azul/zulu-openjdk-alpine:11
COPY --from=build /usr/app/target/charge-service-0.0.1-SNAPSHOT.jar /usr/app/charge-service-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/app/charge-service-0.0.1-SNAPSHOT.jar"]
