package br.com.itau.chargeservice.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "mailer")
@Data
public class MailerProperties {

    private String host;
    private String port;
    private String ssl;
    private String tls;
    private String user;
    private String password;
}
