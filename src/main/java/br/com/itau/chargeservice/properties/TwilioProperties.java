package br.com.itau.chargeservice.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "twilio")
@Data
public class TwilioProperties {

    private String account;
    private String token;
    private String number;
}
