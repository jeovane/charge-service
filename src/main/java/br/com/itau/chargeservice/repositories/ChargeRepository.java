package br.com.itau.chargeservice.repositories;

import br.com.itau.chargeservice.models.Charge;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChargeRepository extends MongoRepository<Charge, String> {

    List<Charge> findAllByClient_Id(String clientId);
}
