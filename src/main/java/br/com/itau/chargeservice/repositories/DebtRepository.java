package br.com.itau.chargeservice.repositories;

import br.com.itau.chargeservice.models.Debt;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DebtRepository extends MongoRepository<Debt, String> {
}
