package br.com.itau.chargeservice.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Data
@Document("changes")
public class Charge {

    @Id
    private String id;
    private Client client;
    private ChargeType type;
    private LocalDate chargeDate;
    private Debt debt;
    private String content;
}
