package br.com.itau.chargeservice.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Document("debts")
@AllArgsConstructor
public class Debt {

    @Id
    private String id;
    private String product;
    private BigDecimal value;
    private LocalDate startDate;
    private LocalDate endDate;
    private Client client;
    private double interest;
}
