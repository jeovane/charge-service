package br.com.itau.chargeservice.models;

public enum ChargeType {

    SMS,
    EMAIL,
    LETTER
}
