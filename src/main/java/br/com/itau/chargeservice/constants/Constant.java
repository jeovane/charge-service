package br.com.itau.chargeservice.constants;

public class Constant {
    public static final String STATUS_200_OK = "Successfully create. ";
    public static final String STATUS_400_BAD_REQUEST = "Resource is invalid.";
    public static final String STATUS_500_INTERNAL_SERVER_ERROR = "Internal server error.";

    public static final String CHARGE_ALREADY_EXIST = "The client %s was already charged today.";

    public static final String CLIENT_NOT_FOUND = "Client not found.";
    public static final String DEBT_NOT_FOUND = "Debt not found.";
    public static final String CHARGE_NOT_FOUND = "Charge not found.";

    public static final String REQUEST_TRACE_ID_NAME = "requestTraceId";
    public static final String REQUEST_TRACE_ID_VALUE = "Cross transaction unique ID.";
    public static final String REQUEST_TRACE_ID_PARAM_TYPE = "header";
    public static final String REQUEST_TRACE_ID_DATA_TYPE = "string";

}
