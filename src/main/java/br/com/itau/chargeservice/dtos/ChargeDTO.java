package br.com.itau.chargeservice.dtos;

import br.com.itau.chargeservice.models.Charge;
import br.com.itau.chargeservice.models.ChargeType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ChargeDTO {

    @ApiModelProperty(
            value = "The client identifier.",
            example = "93f6522e-7b95-11ec-90d6-0242ac120003",
            required = true)
    @NotBlank(message = "Must not be null or empty.")
    private String clientId;
    @ApiModelProperty(
            value = "The type of change.",
            example = "SMS",
            required = true)
    @NotBlank(message = "Must not be null or empty.")
    private String type;

    @ApiModelProperty(
            value = "The debt identifier.",
            example = "77f6522e-7b95-11ec-90d6-0742ac120003",
            required = true)
    @NotBlank(message = "Must not be null or empty.")
    private String debtId;

    @ApiModelProperty(
            value = "The charge content.",
            example = "Dear John, we are sending you a link to pay your bank slip.",
            required = true)
    @NotBlank(message = "Must not be null or empty.")
    private String content;

    public Charge toCharge() {
        Charge charge = new Charge();
        charge.setType(ChargeType.valueOf(this.type.toUpperCase()));
        charge.setContent(this.content);
        return charge;
    }
}
