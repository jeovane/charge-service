package br.com.itau.chargeservice.configs;

import br.com.itau.chargeservice.models.Charge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.Index;

import javax.annotation.PostConstruct;

@Configuration
@DependsOn("mongoTemplate")
public class MongoConfig {

  private MongoTemplate mongoTemplate;

  @Autowired
  public MongoConfig(MongoTemplate mongoTemplate) {
    this.mongoTemplate = mongoTemplate;
  }

  @PostConstruct
  public void initIndexes() {
    mongoTemplate.indexOps(Charge.class).dropAllIndexes();
    mongoTemplate.indexOps(Charge.class).ensureIndex(new Index().on("chargeDate", Sort.Direction.ASC).unique());
  }
}
