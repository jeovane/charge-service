package br.com.itau.chargeservice.configs;

import br.com.itau.chargeservice.exceptions.DuplicateDocumentException;
import br.com.itau.chargeservice.models.Charge;
import br.com.itau.chargeservice.models.Client;
import br.com.itau.chargeservice.models.Debt;
import br.com.itau.chargeservice.services.ClientService;
import br.com.itau.chargeservice.services.DebtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Component
public class DataLoader {

    private final ClientService clientService;
    private final DebtService debtService;
    private final MongoTemplate mongoTemplate;

    @Autowired
    public DataLoader(ClientService clientService, DebtService debtService, MongoTemplate mongoTemplate) {
        this.clientService = clientService;
        this.debtService = debtService;
        this.mongoTemplate = mongoTemplate;
    }

    @PostConstruct
    public void loadData() {
        var client = new Client("123", "John Due", "jeovanebc@gmail.com", "+5582999218815");
        var debt = new Debt("321", "Credit Card", BigDecimal.valueOf(4000),
                LocalDate.now().minusDays(5),
                LocalDate.now().plusDays(25), client, 10);

        try {
            clientService.create(client);
            debtService.create(List.of(debt));
        } catch (DuplicateDocumentException ex){}
    }

    @PreDestroy
    public void removeData() {
        mongoTemplate.remove(new Query(), Charge.class);
        mongoTemplate.remove(new Query(), Client.class);
        mongoTemplate.remove(new Query(), Debt.class);
    }
}