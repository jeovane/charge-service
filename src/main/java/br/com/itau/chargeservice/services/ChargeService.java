package br.com.itau.chargeservice.services;

import br.com.itau.chargeservice.dtos.ChargeDTO;
import br.com.itau.chargeservice.exceptions.DuplicateDocumentException;
import br.com.itau.chargeservice.models.Charge;
import br.com.itau.chargeservice.models.Client;
import br.com.itau.chargeservice.models.Debt;
import br.com.itau.chargeservice.repositories.ChargeRepository;
import br.com.itau.chargeservice.services.notification.NotificationFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static br.com.itau.chargeservice.constants.Constant.CHARGE_ALREADY_EXIST;

@Service
public class ChargeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChargeService.class);

    private final ChargeRepository chargeRepository;
    private final ClientService clientService;
    private final DebtService debtService;
    private final NotificationFactory notificationFactory;

    @Autowired
    public ChargeService(ChargeRepository chargeRepository, ClientService clientService, DebtService debtService, NotificationFactory notificationFactory) {
        this.chargeRepository = chargeRepository;
        this.clientService = clientService;
        this.debtService = debtService;
        this.notificationFactory = notificationFactory;
    }

    public Charge charge(ChargeDTO chargeDTO) {
        Charge charge = chargeDTO.toCharge();
        Client client = clientService.findById(chargeDTO.getClientId());
        Debt debt = debtService.findById(chargeDTO.getDebtId());
        charge.setId(UUID.randomUUID().toString());
        charge.setChargeDate(LocalDate.now());
        charge.setClient(client);
        charge.setDebt(debt);
        try {
            LOGGER.info("Notifying client {}.", client.getName());
            notificationFactory.getNotificationInstance(charge.getType()).send(charge);
            return chargeRepository.save(charge);
        } catch (DuplicateKeyException e) {
            throw new DuplicateDocumentException(String.format(CHARGE_ALREADY_EXIST, client.getName()));
        }

    }

    public List<Charge> findByClientId(String clientId) {
        return chargeRepository.findAllByClient_Id(clientId);
    }
}
