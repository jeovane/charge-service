package br.com.itau.chargeservice.services;

import br.com.itau.chargeservice.exceptions.NotFoundException;
import br.com.itau.chargeservice.models.Debt;
import br.com.itau.chargeservice.repositories.DebtRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static br.com.itau.chargeservice.constants.Constant.DEBT_NOT_FOUND;

@Service
public class DebtService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DebtService.class);

    private final DebtRepository debtRepository;

    @Autowired
    public DebtService(DebtRepository debtRepository) {
        this.debtRepository = debtRepository;
    }

    public List<Debt> create(List<Debt> debts) {
        return debtRepository.saveAll(debts);
    }

    public Debt findById(String debtId) {
        LOGGER.info("Debt Request: {}", debtId);
        return debtRepository.findById(debtId).orElseThrow(() -> new NotFoundException(DEBT_NOT_FOUND));
    }
}