package br.com.itau.chargeservice.services.notification;

import br.com.itau.chargeservice.models.Charge;
import br.com.itau.chargeservice.properties.TwilioProperties;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SmsService implements NotificationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmsService.class);

    private final TwilioProperties properties;

    @Autowired
    public SmsService(TwilioProperties properties) {
        this.properties = properties;
    }

    @Override
    public void send(Charge charge) {
        try {
            LOGGER.info("Sending SMS to {}", charge.getClient().getName());
            Twilio.init(properties.getAccount(), properties.getToken());
            Message.creator(
                            new com.twilio.type.PhoneNumber(charge.getClient().getPhoneNumber()),
                            new com.twilio.type.PhoneNumber(properties.getNumber()),
                            charge.getContent())
                    .create();
        } catch (Exception e) {
            throw new RuntimeException("Error while sending a SMS.", e);
        }
        LOGGER.info("SMS sent successfully");
    }
}