package br.com.itau.chargeservice.services;

import br.com.itau.chargeservice.exceptions.NotFoundException;
import br.com.itau.chargeservice.models.Client;
import br.com.itau.chargeservice.repositories.ClientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static br.com.itau.chargeservice.constants.Constant.CLIENT_NOT_FOUND;

@Service
public class ClientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientService.class);
    private final ClientRepository clientRepository;

    @Autowired
    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public Client findById(String clientId) {
        LOGGER.info("Client Request: {}", clientId);
        return clientRepository.findById(clientId).orElseThrow(() -> new NotFoundException(CLIENT_NOT_FOUND));
    }

    public Client create(Client client) {
        return clientRepository.save(client);
    }
}
