package br.com.itau.chargeservice.services.notification;

import br.com.itau.chargeservice.models.Charge;

public interface NotificationService {

    void send(Charge charge);
}
