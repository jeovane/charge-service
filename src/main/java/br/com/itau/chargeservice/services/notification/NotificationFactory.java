package br.com.itau.chargeservice.services.notification;

import br.com.itau.chargeservice.models.ChargeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NotificationFactory {

    private final SmsService smsService;
    private final EmailService emailService;
    private final LetterService letterService;

    @Autowired
    public NotificationFactory(SmsService smsService, EmailService emailService, LetterService letterService) {
        this.smsService = smsService;
        this.emailService = emailService;
        this.letterService = letterService;
    }

    public NotificationService getNotificationInstance(ChargeType type) {
        if (type.equals(ChargeType.LETTER)) {
            return this.letterService;
        } else if (type.equals(ChargeType.SMS)) {
            return this.smsService;
        } else {
            return this.emailService;
        }
    }
}