package br.com.itau.chargeservice.services.notification;

import br.com.itau.chargeservice.models.Charge;
import br.com.itau.chargeservice.properties.MailerProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class EmailService implements NotificationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    private final MailerProperties mailerProperties;

    @Autowired
    public EmailService(MailerProperties mailerProperties) {
        this.mailerProperties = mailerProperties;
    }

    @Override
    public void send(Charge charge) {
        String from = "jeovanebc@gmail.com";
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", mailerProperties.getHost());
        props.put("mail.smtp.port", mailerProperties.getPort());
        Session session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(mailerProperties.getUser(), mailerProperties.getPassword());
            }
        });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(charge.getClient().getEmail()));
            message.setSubject("Test!");
            message.setText(charge.getContent());
            Transport.send(message);
            LOGGER.info("Email Message Sent Successfully");
        } catch (MessagingException e) {
            throw new RuntimeException("Error while sending a e-mail.", e);
        }
    }
}
