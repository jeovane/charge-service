package br.com.itau.chargeservice.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FieldErrorMessage {

  private String field;
  private String message;

}
