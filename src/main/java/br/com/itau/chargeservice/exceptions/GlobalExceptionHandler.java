package br.com.itau.chargeservice.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(DuplicateDocumentException.class)
  protected ResponseEntity duplicateDocumentException(DuplicateDocumentException ex) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new StandardError(ex.getMessage()));
  }

  @ExceptionHandler(NotFoundException.class)
  protected ResponseEntity duplicateDocumentException(NotFoundException ex) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new StandardError(ex.getMessage()));
  }

  @Override
  protected ResponseEntity handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    List<FieldErrorMessage> errorMessages = ex.getBindingResult().getFieldErrors().stream()
        .map(fieldError -> new FieldErrorMessage(fieldError.getField(), fieldError.getDefaultMessage()))
        .collect(Collectors.toList());
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessages);
  }


}
