package br.com.itau.chargeservice.exceptions;

public class DuplicateDocumentException extends RuntimeException {

  public DuplicateDocumentException(String message) {
    super(message);
  }
}
