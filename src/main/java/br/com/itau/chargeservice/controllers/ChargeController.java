package br.com.itau.chargeservice.controllers;


import br.com.itau.chargeservice.dtos.ChargeDTO;
import br.com.itau.chargeservice.services.ChargeService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static br.com.itau.chargeservice.constants.Constant.*;

@RestController
@RequestMapping("/charges")
public class ChargeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChargeController.class);
    private final ChargeService chargeService;

    @Autowired
    public ChargeController(ChargeService chargeService) {
        this.chargeService = chargeService;
    }

    @ApiOperation("Charge a client.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = STATUS_200_OK),
                    @ApiResponse(code = 400, message = STATUS_400_BAD_REQUEST),
                    @ApiResponse(code = 503, message = STATUS_500_INTERNAL_SERVER_ERROR)
            })
    @PostMapping(produces = "application/json")
    @ApiImplicitParams({
            @ApiImplicitParam(
                    name = REQUEST_TRACE_ID_NAME,
                    value = REQUEST_TRACE_ID_VALUE,
                    paramType = REQUEST_TRACE_ID_PARAM_TYPE,
                    dataType = REQUEST_TRACE_ID_DATA_TYPE,
                    required = true
            )
    })
    public ResponseEntity charge(@RequestBody @Valid ChargeDTO chargeDTO) {
        LOGGER.info("Charge POST Request: {}", chargeDTO);
        return ResponseEntity.status(HttpStatus.OK).body(chargeService.charge(chargeDTO));
    }

    @ApiOperation("Find charge by client identifier.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = STATUS_200_OK),
                    @ApiResponse(code = 400, message = STATUS_400_BAD_REQUEST),
                    @ApiResponse(code = 503, message = STATUS_500_INTERNAL_SERVER_ERROR)
            })
    @GetMapping("clients/{clientId}")
    @ApiImplicitParams({
            @ApiImplicitParam(
                    name = REQUEST_TRACE_ID_NAME,
                    value = REQUEST_TRACE_ID_VALUE,
                    paramType = REQUEST_TRACE_ID_PARAM_TYPE,
                    dataType = REQUEST_TRACE_ID_DATA_TYPE,
                    required = true
            )
    })
    public ResponseEntity findByClientId(@PathVariable String clientId) {
        return ResponseEntity.status(HttpStatus.OK).body(chargeService.findByClientId(clientId));
    }
}