package br.com.itau.chargeservice;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.restassured.RestAssured;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.testcontainers.containers.DockerComposeContainer;

import java.io.File;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/features",
		glue = {"br/com/itau/chargeservice/e2e/config", "br/com/itau/chargeservice/e2e/steps", "br/com/itau/chargeservice/e2e/hooks"},
		plugin = {"pretty", "summary", "html:target/cucumber"})
public class ChargeServiceApplicationTests {
	private static final Integer API_PORT = 8080;
	private static final String BASE_URL = "http://localhost";

	@ClassRule
	public static DockerComposeContainer container =
			new DockerComposeContainer(new File("src/test/resources/docker-compose.yml"));


	@BeforeClass
	public static void beforeSetup() {
		RestAssured.port = API_PORT;
		RestAssured.basePath = BASE_URL;
	}

	@AfterClass
	public static void afterSetup() {
		RestAssured.reset();
	}

}
