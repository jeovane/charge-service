package br.com.itau.chargeservice.services;

import br.com.itau.chargeservice.dtos.ChargeDTO;
import br.com.itau.chargeservice.exceptions.NotFoundException;
import br.com.itau.chargeservice.models.Charge;
import br.com.itau.chargeservice.models.ChargeType;
import br.com.itau.chargeservice.models.Client;
import br.com.itau.chargeservice.models.Debt;
import br.com.itau.chargeservice.repositories.ChargeRepository;
import br.com.itau.chargeservice.services.notification.NotificationFactory;
import br.com.itau.chargeservice.services.notification.SmsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import static br.com.itau.chargeservice.constants.Constant.CLIENT_NOT_FOUND;
import static br.com.itau.chargeservice.constants.Constant.DEBT_NOT_FOUND;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ChargeServiceTest {

    @Mock private ChargeRepository chargeRepository;
    @Mock private ClientService clientService;
    @Mock private DebtService debtService;
    @Mock private NotificationFactory notificationFactory;
    @Mock private SmsService smsService;
    @InjectMocks private ChargeService chargeService;

    @Nested
    class chargeTest {

        @Nested
        abstract class DescribeCharge {

            Charge createdCharge;

            @BeforeEach
            void run() {
                mocks();
                createdCharge = chargeService.charge(givenChargeDTO());
            }

            ChargeDTO givenChargeDTO() {
                ChargeDTO chargeDTO = new ChargeDTO();
                chargeDTO.setClientId("123");
                chargeDTO.setType("SMS");
                chargeDTO.setDebtId("321");
                chargeDTO.setContent("Dear John, we are sending you a link to pay your bank slip.");
                return chargeDTO;
            }

            Client givenClient() {
                return new Client("123", "John", "john@test.com", "8556548548");
            }

            Debt givenDebt() {
                return new Debt("321", "Credit Card", BigDecimal.valueOf(4000),
                        LocalDate.now().minusDays(5),
                        LocalDate.now().plusDays(25), givenClient(), 10);
            }

            Charge givenCharge() {
                Charge charge = new Charge();
                charge.setDebt(givenDebt());
                charge.setClient(givenClient());
                charge.setId(UUID.randomUUID().toString());
                charge.setType(ChargeType.SMS);
                charge.setChargeDate(LocalDate.now());
                charge.setContent("Dear John, we are sending you a link to pay your bank slip.");
                return charge;
            }

            void mocks() {
                when(clientService.findById("123")).thenReturn(givenClient());
                when(debtService.findById("321")).thenReturn(givenDebt());
                when(notificationFactory.getNotificationInstance(ChargeType.SMS)).thenReturn(smsService);
                when(chargeRepository.save(any(Charge.class))).thenReturn(givenCharge());
            }
        }

        @Nested
        class WhenChargeClientSuccessfully extends DescribeCharge  {

            @Test
            void itShouldReturnCreatedCharge() {
                Assertions.assertNotNull(createdCharge);
                Assertions.assertNotNull(createdCharge.getId());
                Assertions.assertEquals(createdCharge.getChargeDate(), LocalDate.now());
                Assertions.assertEquals(createdCharge.getClient().getId(),"123");
                Assertions.assertEquals(createdCharge.getDebt().getId(), "321");
                Assertions.assertEquals(createdCharge.getContent(), "Dear John, we are sending you a link to pay your bank slip.");
            }

            @Test
            void itShouldCallTheFollowingService() {
                verify(chargeRepository, times(1)).save(any());
            }
        }

        @Nested
        class WhenChargeUnregisteredClient extends DescribeCharge  {

            @Override
            @BeforeEach
            void run() {
                when(clientService.findById("123")).thenThrow(new NotFoundException(CLIENT_NOT_FOUND));
            }

            @Test
            void itShouldThrowNotFoundException() {
                NotFoundException exception =
                        Assertions.assertThrows(NotFoundException.class, () -> chargeService.charge(givenChargeDTO()));
                Assertions.assertEquals(exception.getMessage(), "Client not found.");
            }
        }

        @Nested
        class WhenChargeUnregisteredDebt extends DescribeCharge  {

            @Override
            @BeforeEach
            void run() {
                when(debtService.findById("321")).thenThrow(new NotFoundException(DEBT_NOT_FOUND));
            }

            @Test
            void itShouldThrowNotFoundException() {
                NotFoundException exception =
                        Assertions.assertThrows(NotFoundException.class, () -> chargeService.charge(givenChargeDTO()));
                Assertions.assertEquals(exception.getMessage(), "Debt not found.");
            }
        }
    }
}
