package br.com.itau.chargeservice.e2e.hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.restassured.RestAssured;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

public class Hook {

    private final MongoTemplate mongoTemplate;

    @Autowired
    public Hook(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Before
    public void beforeSetup() {
        Query query = new Query();
        mongoTemplate.remove(query, "charges");
        mongoTemplate.remove(query, "clients");
        mongoTemplate.remove(query, "debts");
    }

    @After
    public void afterSetup() {
        RestAssured.reset();
    }
}
