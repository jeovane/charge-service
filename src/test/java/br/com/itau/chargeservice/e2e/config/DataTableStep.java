package br.com.itau.chargeservice.e2e.config;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.DefaultDataTableCellTransformer;
import io.cucumber.java.DefaultDataTableEntryTransformer;
import io.cucumber.java.DefaultParameterTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import java.lang.reflect.Type;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Scope(SCOPE_CUCUMBER_GLUE)
public class DataTableStep {

  @Autowired
  private ObjectMapper objectMapper;

  @DefaultParameterTransformer
  @DefaultDataTableEntryTransformer(replaceWithEmptyString = "[blank]")
  @DefaultDataTableCellTransformer(replaceWithEmptyString = "[blank]")
  public Object defaultTransformer(final Object fromValue, final Type toValueType) {
    final JavaType javaType = objectMapper.constructType(toValueType);
    return objectMapper.convertValue(fromValue, javaType);
  }
}
