package br.com.itau.chargeservice.e2e.dtos;

import lombok.Data;

@Data
public class MessageDTO {

  private String message;
}
