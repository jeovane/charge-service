package br.com.itau.chargeservice.e2e.steps;

import br.com.itau.chargeservice.e2e.dtos.MessageDTO;
import br.com.itau.chargeservice.exceptions.FieldErrorMessage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;

public class CommonSteps<T> {

    private Response response;
    private T body;

    public Response getResponse() {
        return response;
    }

    public void setBody(T body) {
        this.body = body;
    }


    @Then("The response should have {int} as status code")
    public void theResponseShouldHaveAsStatusCode(int code) {
        Assertions.assertEquals(response.statusCode(), code);
    }

    @When("I make a {string} request to endpoint {string}")
    public void iMakeARequestToCreateRole(String httpMethod, String path) {
        switch (httpMethod) {
            case "post": {
                response = given()
                        .header("Content-type", "application/json")
                        .and()
                        .body(body)
                        .when()
                        .post(path)
                        .then()
                        .extract().response();
                break;
            }
            case "get": {
                response = given()
                        .header("Content-type", "application/json")
                        .and()
                        .when()
                        .get(path)
                        .then()
                        .extract().response();
                break;
            }
        }
    }

    @And("The response should have the following field error message(s)")
    public void theBodyResponseShouldHaveTheFollowingFieldErrorMessage(List<FieldErrorMessage> messages) {
        List<FieldErrorMessage> actualMessages = Arrays.asList(this.getResponse().as(FieldErrorMessage[].class));
        for (int i = 0; i < messages.size(); i++) {
            FieldErrorMessage expected = messages.get(i);
            FieldErrorMessage actual = actualMessages.get(i);
            Assertions.assertEquals(expected.getField(), actual.getField());
            Assertions.assertEquals(expected.getMessage(), actual.getMessage());
        }
    }

    @And("The response should have the following error message")
    public void theBodyResponseShouldHaveTheFollowingErrorMessage(MessageDTO expected) {
        MessageDTO actual = response.as(MessageDTO.class);
        Assertions.assertEquals(actual.getMessage(), expected.getMessage());
    }
}
