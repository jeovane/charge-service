package br.com.itau.chargeservice.e2e.steps;

import br.com.itau.chargeservice.dtos.ChargeDTO;
import br.com.itau.chargeservice.models.Charge;
import br.com.itau.chargeservice.models.Client;
import br.com.itau.chargeservice.models.Debt;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

public class ChargeSteps {

    private final CommonSteps<ChargeDTO> commonStep;
    private final MongoTemplate mongoTemplate;

    @Autowired
    public ChargeSteps(CommonSteps<ChargeDTO> commonStep, MongoTemplate mongoTemplate) {
        this.commonStep = commonStep;
        this.mongoTemplate = mongoTemplate;
    }

    @Given("I have the following client")
    public void iHaveTheFollowingClient(Client client) {
        this.mongoTemplate.save(client, "clients");
    }

    @And("I have the following debt to client with id {string}")
    public void iHaveTheFollowingDebtToClientWithId(String clientId, Debt debt) {
        Criteria criteria = new Criteria().andOperator(Criteria.where("id").is(clientId));
        Client client = this.mongoTemplate.findOne(new Query().addCriteria(criteria), Client.class, "clients");
        if (client == null) {
            throw new RuntimeException("Client does not exist");
        }
        debt.setClient(client);
        this.mongoTemplate.save(debt, "debts");
    }

    @And("I have the following charge body request")
    public void iHaveTheFollowingChargeBodyRequest(ChargeDTO chargeDTO) {
        commonStep.setBody(chargeDTO);
    }

    @And("The response should have the following charge")
    public void theResponseShouldHaveTheFollowingCharge(Charge expected) {
        Charge charge = commonStep.getResponse().as(Charge.class);
        Assertions.assertEquals(expected.getType(), charge.getType());
        Assertions.assertEquals(expected.getContent(), charge.getContent());
    }

    @And("The charge should have the following client")
    public void theChargeShouldHaveTheFollowingClient(Client client) {
        Charge charge = commonStep.getResponse().as(Charge.class);
        Assertions.assertEquals(client, charge.getClient());
    }

    @And("The charge should have the following debt")
    public void theChargeShouldHaveTheFollowingDebt(Debt debt) {
        Charge charge = commonStep.getResponse().as(Charge.class);
        Debt actual = charge.getDebt();
        Assertions.assertEquals(debt.getId(), actual.getId());
        Assertions.assertEquals(debt.getInterest(), actual.getInterest());
        Assertions.assertEquals(debt.getProduct(), actual.getProduct());
        Assertions.assertEquals(debt.getValue(), actual.getValue());
        Assertions.assertEquals(debt.getStartDate(), actual.getStartDate());
        Assertions.assertEquals(debt.getEndDate(), actual.getEndDate());
    }

    @And("The charge should have a not blank id")
    public void theChargeShouldHaveANotBlankId() {
        Charge charge = commonStep.getResponse().as(Charge.class);
        Assertions.assertNotNull(charge.getId());
    }
}
