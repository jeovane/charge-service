Feature: Create a charge

  Scenario Outline: Creating a new charge with unregistered client/debt
    Given I have the following client
      | id  | name     | email         | phoneNumber    |
      | 123 | John Due | joh@gmail.com | +5582999218815 |
    And I have the following debt to client with id "123"
      | id  | product     | value | startDate  | endDate    | interest |
      | 321 | Credit Card | 14000 | 2022-11-01 | 2022-12-01 | 10       |
    And I have the following charge body request
      | clientId   | type | debtId   | content                                                     |
      | <clientId> | SMS  | <debtId> | Dear John, we are sending you a link to pay your bank slip. |
    When I make a "post" request to endpoint "/charges"
    Then The response should have 400 as status code

    Examples:
      | clientId | debtId |
      | 123456   | 321    |
      | 123      | 321456 |

  Scenario Outline: Creating a new charge with invalid body request
    Given I have the following client
      | id  | name     | email         | phoneNumber    |
      | 123 | John Due | joh@gmail.com | +5582999218815 |
    And I have the following debt to client with id "123"
      | id  | product     | value | startDate  | endDate    | interest |
      | 321 | Credit Card | 14000 | 2022-11-01 | 2022-12-01 | 10       |
    And I have the following charge body request
      | clientId   | type   | debtId   | content   |
      | <clientId> | <type> | <debtId> | <content> |
    When I make a "post" request to endpoint "/charges"
    Then The response should have 400 as status code
    And The response should have the following field error messages
      | field   | message   |
      | <field> | <message> |

    Examples:
      | clientId | type    | debtId  | content                   | field    | message                    |
      |          | SMS     | 321     | Pleas, pay you bank slip. | clientId | Must not be null or empty. |
      | [blank]  | SMS     | 321     | Pleas, pay you bank slip. | clientId | Must not be null or empty. |
      | 123      |         | 321     | Pleas, pay you bank slip. | type     | Must not be null or empty. |
      | 123      | [blank] | 321     | Pleas, pay you bank slip. | type     | Must not be null or empty. |
      | 123      | SMS     |         | Pleas, pay you bank slip. | debtId   | Must not be null or empty. |
      | 123      | SMS     | [blank] | Pleas, pay you bank slip. | debtId   | Must not be null or empty. |
      | 123      | SMS     | 321     |                           | content  | Must not be null or empty. |
      | 123      | SMS     | 321     | [blank]                   | content  | Must not be null or empty. |

  Scenario: Creating a new charge successfully
    Given I have the following client
      | id  | name     | email         | phoneNumber    |
      | 123 | John Due | joh@gmail.com | +5582999218815 |
    And I have the following debt to client with id "123"
      | id  | product     | value | startDate  | endDate    | interest |
      | 321 | Credit Card | 14000 | 2022-11-01 | 2022-12-01 | 10       |
    And I have the following charge body request
      | clientId | type | debtId | content                                                     |
      | 123      | SMS  | 321    | Dear John, we are sending you a link to pay your bank slip. |
    When I make a "post" request to endpoint "/charges"
    Then The response should have 200 as status code
    And The response should have the following charge
      | type | content                                                     |
      | SMS  | Dear John, we are sending you a link to pay your bank slip. |
    And The charge should have the following client
      | id  | name     | email         | phoneNumber    |
      | 123 | John Due | joh@gmail.com | +5582999218815 |
    And The charge should have the following debt
      | id  | product     | value | startDate  | endDate    | interest |
      | 321 | Credit Card | 14000 | 2022-11-01 | 2022-12-01 | 10       |
    And The charge should have a not blank id
